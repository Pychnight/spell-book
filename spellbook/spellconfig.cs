﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace SpellBook
{
    public class Spellconfig
    {

        /// <summary>
        /// Reads a configuration file from a given path
        /// </summary>
        /// <param name="path">string path</param>
        /// <returns>ConfigFile object</returns>
        public static Spellconfig Read(string path)
        {
            if (!File.Exists(path))
                return new Spellconfig();
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return Read(fs);
            }
        }

        /// <summary>
        /// Reads the configuration file from a stream
        /// </summary>
        /// <param name="stream">stream</param>
        /// <returns>ConfigFile object</returns>
        public static Spellconfig Read(Stream stream)
        {
            using (var sr = new StreamReader(stream))
            {
                var cf = JsonConvert.DeserializeObject<Spellconfig>(sr.ReadToEnd());

                if (ConfigRead != null)
                    ConfigRead(cf);
                return cf;
            }
        }

        /// <summary>
        /// Writes the configuration to a given path
        /// </summary>
        /// <param name="path">string path - Location to put the config file</param>
        public void Write(string path)
        {
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                Write(fs);
            }
        }

        /// <summary>
        /// Writes the configuration to a stream
        /// </summary>
        /// <param name="stream">stream</param>
        public void Write(Stream stream)
        {
            var projectiles = new List<Spell>();

            var spell1 = new Spell
                (10, //ID
                    "NAME",
                    10, //DAMAGE
                    0, //CordX
                    0, //CordY
                    100, //SpeedX
                    100, //SpeedY
                    50, //KnockBack
                    false, //look for target
                    true, //Colide with tiles?
                    0, // AI Slot 0
                    0, // AI Slot 1
                    "Permission", // Permission
                    "Difficulty");

            projectiles.Add(spell1);

            var str = JsonConvert.SerializeObject(this, Formatting.Indented);
            using (var sw = new StreamWriter(stream))
            {
                sw.Write(str);
            }
        }

        /// <summary>
        /// On config read hook
        /// </summary>
        public static Action<Spellconfig> ConfigRead;
    }
}