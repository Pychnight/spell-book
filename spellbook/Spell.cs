﻿using Terraria;

namespace SpellBook
{
    public class Spell
    {
        public NPC MainNpc { get; set; }
        public int ProjectileId { get; set; }
        public string ProjectileName { get; set; }
        public int ProjectileDamage { get; set; }
        public int ProjectileCordX { get; set; }
        public int ProjectileCordY { get; set; }
        public int ProjectileSpeedX { get; set; }
        public int ProjectileSpeedY { get; set; }
        public int ProjectileKb { get; set; }
        public bool ProjectileCheckCollision { get; set; }
        public float ProjectileAiParams1 { get; set; }
        public float ProjectileAiParams2 { get; set; }
        public bool ProjectileLookForTarget { get; set; }
        public string Permission { get; set; }
        public string Difficulty { get; set; }

        public Spell(int projectileid, string projectilename, int projectiledamage, int projectilecordx, int projectilecordy, int projectilespeedx, int projectilespeedy, int projectilekb, bool projectilelookfortarget = false, bool projectilecheckcollision = true, float projectileaiparams1 = 0f, float projectileaiparams2 = 0f, string permission = "", string difficulty = "")
        {
            MainNpc = MainNpc;
            ProjectileId = projectileid;
            ProjectileName = projectilename;
            ProjectileDamage = projectiledamage;
            ProjectileCordX = projectilecordx;
            ProjectileCordY = projectilecordy;
            ProjectileSpeedX = projectilespeedx;
            ProjectileSpeedY = projectilespeedy;
            ProjectileKb = projectilekb;
            ProjectileCheckCollision = projectilecheckcollision;
            ProjectileAiParams1 = projectileaiparams1;
            ProjectileAiParams2 = projectileaiparams2;
            ProjectileLookForTarget = projectilelookfortarget;
            ProjectileLookForTarget = projectilelookfortarget;
            Permission = permission;
            Difficulty = difficulty;
        }
    }
}
