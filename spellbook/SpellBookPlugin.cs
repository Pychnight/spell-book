﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Timers;
using Mono.Data.Sqlite;
using MySql.Data.MySqlClient;
using Terraria;
using TerrariaApi.Server;
using TShockAPI;
using TShockAPI.DB;

namespace SpellBook
{
    [ApiVersion(1, 20)]
    public class SpellBookPlugin : TerrariaPlugin
    {

    private Timer _mainLoop = new Timer(1000 / 60.0);
    public Spellconfig ConfigObj { get; set; }
    internal static string Filepath { get { return Path.Combine(TShock.SavePath, "spellbook.json"); } }
    private IDbConnection _database;

        // used by the magic learning system
    private List<string> _magic = new List<string>();
    private List<string> _magiclearned = new List<string>();

    //don't ask.
    private IList<Spell> _projectiles = new List<Spell>();
    internal static NPC[] NpCs = new NPC[200];
   private int _npcIndex;
   //private int _projectileIndex;
    private int _costMana;

    //used by the plugin
    private DateTime?[] _timer = new DateTime?[Main.maxNPCs];
    int Players { get; }

     //projectile customization stuff
    private int _amountofprojectiles;

        /*
        private int _aiStyle;
        private float _aiItem1;
        private float _aiItem2;
        private int _speedY;
        private int _speedX;
        private float _projX;
        private float _projY;
        private int _projId;
        private string _projName;
        private int _projdmg;
        */

        public SpellBookPlugin(Main game, int players)
        : base(game)
        {
            Players = players;
            //Players = players;
        //_npcIndex = npcIndex;
        //_projectileIndex = projectileIndex;
        //UsingSEConomy = File.Exists(Path.Combine("ServerPlugins", "Wolfje.Plugins.SEconomy.dll"));
        }

        public override string Name
    {
        get
        {
            return "SpellBook";
        }
    }

    public override string Author
    {
        get
        {
            return "Pychnight";
        }
    }

    public override Version Version
    {
        get
        {
            return new Version("1.0");
        }
    }

    public override void Initialize()
    {
        ServerApi.Hooks.GameInitialize.Register(this, OnInitialize);
        ServerApi.Hooks.GameUpdate.Register(this, OnUpdate);
        ServerApi.Hooks.ProjectileAIUpdate.Register(this, AiUpdate);
    }

    void mainLoop_Elapsed(object sender, ElapsedEventArgs e)
    {
        //Don't run this when there is no players
        if (TShock.Utils.ActivePlayers() == 0)
        {
            return;
        }
        //fire projectiles towards closests player
        //ProjectileCheck();
        //Check for player Collision with NPC
        //magictarget();
        Specialmagiccheck();
    }

    private void OnInitialize(EventArgs args)
    {
        Commands.ChatCommands.Add(new Command("spellbook.use", CastMagic, "spellbook", "spb"));
        ConfigObj = new Spellconfig();
        Setupspells();
        _mainLoop.Elapsed += mainLoop_Elapsed;
        _mainLoop.Enabled = true;

        switch (TShock.Config.StorageType.ToLower())
		{
			case "mysql":
				string[] host = TShock.Config.MySqlHost.Split(':');
				_database = new MySqlConnection
				{
					ConnectionString = string.Format("Server={0}; Port={1}; Database={2}; Uid={3}; Pwd={4};",
						host[0],
						host.Length == 1 ? "3306" : host[1],
						TShock.Config.MySqlDbName,
						TShock.Config.MySqlUsername,
						TShock.Config.MySqlPassword)
				};
				break;
			case "sqlite":
				string sql = Path.Combine(TShock.SavePath, "spellbook.sqlite");
				_database = new SqliteConnection(string.Format("uri=file://{0},Version=3", sql));
				break;
		}
		SqlTableCreator sqlcreator = new SqlTableCreator(_database,
			_database.GetSqlType() == SqlType.Sqlite ? (IQueryBuilder)new SqliteQueryCreator() : new MysqlQueryCreator());
		sqlcreator.EnsureTableStructure(new SqlTable("Spellbook",
            new SqlColumn("ID", MySqlDbType.Int32) { AutoIncrement = true, Primary = true },
            new SqlColumn("SpellBook Account", MySqlDbType.Text),
			new SqlColumn("magiclevel", MySqlDbType.Int32),
			new SqlColumn("magiclearned", MySqlDbType.Text),
            new SqlColumn("spellcost", MySqlDbType.Int32)));

        sqlcreator.EnsureTableStructure(new SqlTable("Spells",
            new SqlColumn("ID", MySqlDbType.Int32) { AutoIncrement = true, Primary = true },
            new SqlColumn("Spell Name", MySqlDbType.Text),
			new SqlColumn("Spell Account", MySqlDbType.Text),
			new SqlColumn("splevel", MySqlDbType.Int32),
			new SqlColumn("spexp", MySqlDbType.Int32)));
        }

    public class Player
    {
        public int Index { get; set; }
        public int magiclevel { get; set; }
        public int spelllevel { get; set; }
        public List<string> Magiclearned = new List<string>();
        public TSPlayer TsPlayer { get { return TShock.Players[Index]; } }
        public Player(int index)
        {
            Index = index;
            magiclevel = magiclevel;
            spelllevel = spelllevel;
            Magiclearned = Magiclearned;
        }
    }

    private void Setupspells()
    {
        try
        {
            if (File.Exists(Filepath))
            {
                ConfigObj = new Spellconfig();
                ConfigObj = Spellconfig.Read(Filepath);
            }
            ConfigObj.Write(Filepath);
        }
        catch (Exception ex)
        {
            TShock.Log.ConsoleError(ex.Message);
        }
    }

    private void Debug(TSPlayer player, Spell projectile, NPC npc)
    {
        if (player != null)
        {
            Console.WriteLine(string.Empty);
            Console.WriteLine(player.TPlayer.direction + " Checking direction of Player");
            Console.WriteLine(projectile.MainNpc.name + " Checking Proj " + projectile.MainNpc.whoAmI + " Proj Index");
            Console.WriteLine(projectile.MainNpc.aiStyle + " is Proj Ai Style and " + projectile.MainNpc.direction + " is Projectile current direction");
            Console.WriteLine(projectile.MainNpc.ai[0] + " Proj AI in slot 0 and " + projectile.MainNpc.ai[1] + " Proj AI slot 1");
            Console.WriteLine(string.Empty);
            Console.WriteLine(npc.name + " Checking NPC name " + npc.whoAmI + " NPC Index");
            Console.WriteLine(npc.target + " NPC Target " + npc.direction + " is NPC direction");
            Console.WriteLine(string.Empty);
            Console.WriteLine(projectile.MainNpc.Center.X + " Projectile X " + projectile.MainNpc.Center.Y + " Projectile Y");
            Console.WriteLine(projectile.ProjectileSpeedX + " Proj Speed X " + projectile.ProjectileSpeedY + " Proj Speed Y");
        }
        else
        {
            Console.WriteLine("Player was Null not executing debug code");
        }
    }

    private void OnUpdate(EventArgs args)
    {
       //Database Reading?
       // foreach (Player player in Players )
       // {
       //     _database.QueryReader("SELECT Account, magiclevel, magiclearned");

       // }

        foreach (Spell projectile in _projectiles)
        {
            if ( projectile.MainNpc.active == false || projectile == null )
            {
                _projectiles.Remove(projectile);
            }
        }
    }

    private void AiUpdate(ProjectileAiUpdateEventArgs args)
    {
        foreach (Spell projectile in _projectiles)
        {
            if (projectile.ProjectileName == "Fireball" || projectile.ProjectileName == "Thunder" || projectile.ProjectileName == "Inferno" )
            {
                NetMessage.SendData(27, -1, -1, string.Empty, projectile.ProjectileId);
            }
        }
    }

    private void CastMagic(CommandArgs args)
    {

        if (args.Parameters.Count == 0 || args.Parameters.Count > 1 && args.Player.Group.HasPermission("spellbook"))
        {
            args.Player.SendInfoMessage("Info: /spellbook <spellname>");
            args.Player.SendInfoMessage("Info: /spellbook list for all you currently have learned");
            return;
        }

        if (args.Parameters.Count == 1 && args.Player.Group.HasPermission("spellbook.reload") && args.Message.Contains("reload"))
        {
            args.Player.SendInfoMessage("Reloading Spellbook");
            Setupspells();

            return;
        }

            if (args.Parameters.Count == 1 && args.Player.Group.HasPermission("spellbook") && args.Message.Contains("list"))
        {
            if (_magiclearned.Count == 0 )
            {
                args.Player.SendInfoMessage("You Currently Have no magic learned");
                args.Player.SendInfoMessage("use /spellbook learn to see what magic you can learn");
            }
            return;
        }


        if (args.Parameters.Count == 1 && args.Player.Group.HasPermission("spellbook") && args.Message.Contains("learn"))
        {
            if (args.Player.Group.HasPermission("spb_fireball") && _magiclearned.Contains("fireball") && !_magiclearned.Contains("fireball"))
            {
                args.Player.SendInfoMessage("You Currently Can Learn Fireball" + _magic);
            }
            if (args.Player.Group.HasPermission("spb_fireball") && _magiclearned.Contains("fireball"))
            {
                args.Player.SendInfoMessage("You already have fireball learned");
            }
            return;
        }

        if (args.Parameters.Count == 1 && args.Message.Contains("fireball") || args.Message.Contains("fb") && args.Player.TPlayer.statMana >= 20
            && args.Player.Group.HasPermission("spb_fireball") || args.Player.Group.Name.Contains("superadmin") && _magiclearned.Contains("fireball"))
        {
            Console.WriteLine(args.Player.Name + " has cast Fireball");
            Spell projectile = null;
            Preparespell(args.Player, projectile, args.Message);
 
            _costMana = 20;
            args.Player.TPlayer.statMana -= _costMana;
            NetMessage.SendData(42, -1, -1, "", args.Player.Index);

            return;
        }

        if (args.Parameters.Count == 1 && args.Message.Contains("thunder") && args.Player.TPlayer.statMana >= 45
            && args.Player.Group.HasPermission("spb_thunder") || args.Player.Group.Name.Contains("superadmin") && _magiclearned.Contains("thunder"))
        {
            Console.WriteLine(args.Player.Name + " has cast Thunder");
            Spell projectile = null;
            Preparespell(args.Player, projectile, args.Message);

            _costMana = 45;
             args.Player.TPlayer.statMana -= _costMana;
            NetMessage.SendData(42, -1, -1, "", args.Player.Index);

            return;
        }

        if (args.Parameters.Count == 1 && args.Message.Contains("inferno") && args.Player.TPlayer.statMana >= 150
            && args.Player.Group.HasPermission("spb_inferno") || args.Player.Group.Name.Contains("superadmin") && _magiclearned.Contains("inferno"))
        {
            Console.WriteLine(args.Player.Name + " has cast Inferno");
            Spell projectile = null;
            Preparespell(args.Player, projectile, args.Message);

            _costMana = 150;
            args.Player.TPlayer.statMana -= _costMana;
            NetMessage.SendData(42, -1, -1, "", args.Player.Index);
        }
    }

    private void Preparespell(TSPlayer player, Spell projectile, string message)
    {
        var npc = Main.npc[_npcIndex];
        Console.WriteLine(string.Empty);
        Console.WriteLine("Before Wraping the projectile");
        Debug(player, projectile, npc);

        if (message.Contains("fireball") || message.Contains("fb"))
        {
            //Read from Config in each method

            /*
            _projName = "Fireball";
            _projId = 15;
            _projX = player.TPlayer.Center.X;
            _projY = player.TPlayer.Center.Y;
            _speedX = 10;
            _speedY = 0;
            _projdmg = 150;
            _aiStyle = 11;

            _aiItem1 = 0;
            _aiItem2 = 11;
            */
            _amountofprojectiles = 1;
        }

        if (message.Contains("thunder"))
        {     
            /* 
            _projName = "Thunder";
            _projId = 254;
            _projX = player.TPlayer.Center.X;
            _projY = player.TPlayer.Center.Y;
            _speedX = 10;
            _speedY = 0;
            _projdmg = 400;
            _aiStyle = 5;

            _aiItem1 = 0;
            _aiItem2 = 5;
            */
            _amountofprojectiles = 1;
        }

        if (message.Contains("inferno"))
        {
            /*
            _projName = "Inferno";
            _projId = 85;
            _projX = player.TPlayer.Center.X;
            _projY = player.TPlayer.Center.Y;
            _speedX = 0;
            _speedY = -10;
            _projdmg = 850;
            _aiStyle = 6;

            _aiItem1 = 6;
            _aiItem2 = 14;
            */
            _amountofprojectiles = 8;
        }

        Console.WriteLine("After Wraping the Projectile");
        Debug(player, projectile, npc);


        Magictarget(player, projectile);
    }

    private void Magictarget(TSPlayer player, Spell projectile)
    {
        Console.WriteLine(string.Empty);
        Console.WriteLine("Start of Magic Target Spell Class");
        var npc = Main.npc[_npcIndex];
        Debug(player, projectile, npc);

        if (player.TPlayer.direction == -1)
        {
            projectile.ProjectileCordX =- 20;
            projectile.ProjectileCordY =- 20;
            projectile.ProjectileSpeedX = projectile.ProjectileSpeedX - projectile.ProjectileSpeedX - projectile.ProjectileSpeedX;
            //speedY = speedY;
        }
        else if (player.TPlayer.direction == 1)
        {
            projectile.ProjectileCordX = -20;
        }

        npc.target = player.TPlayer.lastCreatureHit;

        Console.WriteLine(string.Empty);
        Console.WriteLine("Before execution of new projectile");
        Debug(player, projectile, npc);



        for (int i = 0; i <= _amountofprojectiles; i++)
        {
            if (i >= 1)
            {
                Tuple<float, float> ai = Tuple.Create(projectile.ProjectileAiParams1, projectile.ProjectileAiParams2);
                Console.WriteLine(_amountofprojectiles + " Spawning Projectile");
                int projectileIndex = Projectile.NewProjectile(
                    projectile.ProjectileCordX, 
                    projectile.ProjectileCordY, 
                    projectile.ProjectileSpeedX, 
                    projectile.ProjectileSpeedY, 
                    projectile.ProjectileId, 
                    projectile.ProjectileDamage, 
                    projectile.ProjectileKb, 
                    player.Index,
                    ai.Item1,
                    ai.Item2);

                    //Refresh Reseted values

                var proj = Main.projectile[projectileIndex];
                proj.name = projectile.ProjectileName;
                //projectile.MainNpc.aiStyle = projectile.ProjectileAiParams1;
                proj.ai[0] = ai.Item1;
                proj.ai[1] = ai.Item2;

                //_projectiles.Add(projectile);

                NetMessage.SendData(27, player.Index, -1, string.Empty, projectileIndex);       
            }
        }

        _amountofprojectiles = 0;

        Console.WriteLine(string.Empty);
        Console.WriteLine("data has been sent");
        Debug(player, projectile, npc);
    }

    private void Specialmagiccheck()
    {
        TSPlayer player = TShock.Players[Players];
        TSPlayer.All.TPlayer.whoAmI = player.Index;

        foreach (Spell projectile in _projectiles)
        {
            if (projectile.ProjectileName == "Thunder" && projectile.MainNpc.active)
            {
                if (_timer[_npcIndex] == null)
                {
                    _timer[_npcIndex] = DateTime.Now;
                }

                if ((DateTime.Now - _timer[_npcIndex]).Value.TotalSeconds >= 1.5)
                {
                    var npc = Main.npc[_npcIndex];
                    //_projName = "Special Thunder";
                    //_projX = projectile.mainNPC.Center.X;
                    //_projY = projectile.mainNPC.Center.Y;
                    //_speedX = 5;
                    //_speedY = 0;
                    Tuple<float, float> ai = Tuple.Create(projectile.ProjectileAiParams1, projectile.ProjectileAiParams2);

                    int projectileIndex = Projectile.NewProjectile(
                        projectile.ProjectileCordX,
                        projectile.ProjectileCordY,
                        projectile.ProjectileSpeedX,
                        projectile.ProjectileSpeedY,
                        projectile.ProjectileId,
                        projectile.ProjectileDamage,
                        projectile.ProjectileKb,
                        player.Index,
                        ai.Item1,
                        ai.Item2);

                    //Refresh Reseted values

                    var proj = Main.projectile[projectileIndex];
                    proj.name = projectile.ProjectileName;
                    //projectile.MainNpc.aiStyle = projectile.ProjectileAiParams1;
                    proj.ai[0] = ai.Item1;
                    proj.ai[1] = ai.Item2;

                    NetMessage.SendData(27, player.Index, -1, string.Empty, projectileIndex);

                 
                    _amountofprojectiles = 0;

                    _timer[_npcIndex] = DateTime.Now;
                }

                if (projectile.ProjectileName == "Inferno" && projectile.MainNpc.active)
                {
                    if (_timer[_npcIndex] == null)
                    {
                        _timer[_npcIndex] = DateTime.Now;
                    }

                    if ((DateTime.Now - _timer[_npcIndex]).Value.TotalSeconds >= 1)
                    {
                        //_projX = projectile.mainNPC.Center.X;
                        //_projY = projectile.mainNPC.Center.Y;
                        //_projName = "Special Inferno";
                        //_speedX = 5;
                        //_speedY = -10;
                        Tuple<float, float> ai = Tuple.Create(projectile.ProjectileAiParams1, projectile.ProjectileAiParams2);

                        int projectileIndex = Projectile.NewProjectile(
                            projectile.ProjectileCordX,
                            projectile.ProjectileCordY,
                            projectile.ProjectileSpeedX,
                            projectile.ProjectileSpeedY,
                            projectile.ProjectileId,
                            projectile.ProjectileDamage,
                            projectile.ProjectileKb,
                            player.Index,
                            ai.Item1,
                            ai.Item2);

                        //Refresh Reseted values

                        var proj = Main.projectile[projectileIndex];
                        proj.name = projectile.ProjectileName;
                        //projectile.MainNpc.aiStyle = projectile.ProjectileAiParams1;
                        proj.ai[0] = ai.Item1;
                        proj.ai[1] = ai.Item2;

                        _amountofprojectiles = 0;

                        _timer[_npcIndex] = DateTime.Now;
                    }
                }
            }
        }
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {          
            ServerApi.Hooks.GameInitialize.Deregister(this, OnInitialize);
            ServerApi.Hooks.ProjectileAIUpdate.Deregister(this, AiUpdate);
            //ServerApi.Hooks.NetSendData.Deregister(this, OnDataSend);
            ServerApi.Hooks.GameUpdate.Deregister(this, OnUpdate);
        }
    }

    }
}
